var game = {

    startGame: function() {
        this.resetScore();
        UI.enableButtons();
        UI.disableStart();
        simon.startSimon();
    },

    resetScore: function() {
        $('.score').text('0');
    },

    animateButton: function(button) {
        console.log($(this));
    },

    gameOver: function() {
        UI.enableStart();
        UI.disableButtons();
    }
};

var UI = {
    enableButtons: function() {
        $('.button').on('click', function(){
            console.log($(this));
        });
    },

    disableButtons: function() {
        $('.button').off('click');
    },

    enableStart: function() {
        $('.start').prop('disabled', false);
    },

    disableStart: function() {
        $('.start').prop('disabled', true);
    },
};

var simon = {

    // simonSequence holds the array of Simon's selections.
    simonSequence: [],

    startSimon: function() {
        simon.simonSequence = [];
        this.getRandomButton();
    },

    playSimonSequence: function() {
        $.each(this.simonSequence, game.animateButton());
    },

    getRandomNumber: function() {
        return Math.floor(Math.random() * 4);
    },

};

var player = {
    playerChoice: null,

};


(function() {
        $('.start').on('click', function() {
            game.startGame();
        });
})();
